export const filterContainer = document.querySelector(".filter-container");
const selectStatus = document.querySelector(".by-status");
const selectUrgency = document.querySelector(".by-urgency");
const inputName = document.querySelector(".by-name");
const clearFiltersButton = document.querySelector(".clear-filters");
export const emptyFiltersText = document.querySelector(".empty-filters-text")

function showEmptyContainerFilter() {
    const cards = document.querySelectorAll('.card');
    const hiddenCards = [...cards].every(card => card.classList.contains("hide"));

    if (hiddenCards) {
        emptyFiltersText.classList.remove("hide");
        emptyFiltersText.classList.add("show");
    } else {
        emptyFiltersText.classList.remove("show");
        emptyFiltersText.classList.add("hide");
    }
}


function filterByStatus() {
    const date = new Date();
    const selectStatusValue = selectStatus.value;
    const cards = document.querySelectorAll('.card');

    cards.forEach(card => {
        let heading = card.querySelector('.card-date');
        let headingContent = heading.innerHTML;
        const cardDate = new Date(headingContent);

        if (selectStatusValue === "done" && cardDate < date) {
            card.classList.add('show');
            card.classList.remove('hide');
        } else if (selectStatusValue === "inProgress" && cardDate >= date) {
            card.classList.add('show');
            card.classList.remove('hide');
        } else {
            card.classList.add('hide');
            card.classList.remove('show');
        }
    });

    showEmptyContainerFilter()
}

export function filterByName() {
    const inputValue = inputName.value.toUpperCase();
    const cards = document.querySelectorAll('.card');

    cards.forEach(card => {
        let heading = card.querySelector('.card-name');
        let headingContent = heading.innerHTML.toUpperCase();

        if (headingContent.includes(inputValue)) {
            card.classList.add('show');
            card.classList.remove('hide');
        } else {
            card.classList.add('hide');
            card.classList.remove('show');
        }
    });

    showEmptyContainerFilter()
}

function filterByUrgency() {
    const selectUrgencyValue = selectUrgency.value;
    const cards = document.querySelectorAll('.card');

    cards.forEach(info => {
        let heading = info.querySelector('.urgency');
        let headingContent = heading.innerHTML;

        if (headingContent.includes(selectUrgencyValue)) {
            info.classList.add('show');
            info.classList.remove('hide');
        } else {
            info.classList.add('hide');
            info.classList.remove('show');
        }
    });

    showEmptyContainerFilter()
}

clearFiltersButton.addEventListener("click", () => {
    filterContainer.reset();
    filterByName();
});

selectStatus.addEventListener("change", filterByStatus)
selectUrgency.addEventListener("change", filterByUrgency);
inputName.addEventListener('input', filterByName);