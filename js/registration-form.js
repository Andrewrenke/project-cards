import {loadAndDisplayCards} from './fetch.js';
import {filterContainer} from "./filters.js";

const API_URL = "https://ajax.test-danit.com/api/v2/cards/login";
export let API_KEY = "";

const emailInput = document.querySelector(".email");
const passwordInput = document.querySelector(".password");
const submitButton = document.querySelector(".submit-button");
const errorText = document.querySelector(".registration-error");
const logInButton = document.querySelector(".log-in-btn");
const title = document.querySelector(".title");
const regForm = document.querySelector(".registration-form");
const createVisitButton = document.querySelector(".create-visit-btn");
const cardForm = document.getElementById("card-form");
const cardsContainer = document.getElementById("cards-container");
const modal = new bootstrap.Modal(document.getElementById('exampleModal'));
const mainSection = document.querySelector(".main-section")

// Валідація емейлу
function isValidEmail(email) {
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailPattern.test(email);
}

function isToken(token) {
    const tokenPattern = /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/;
    return tokenPattern.test(token);
}

export function saveToken(token) {
    API_KEY = token
    localStorage.setItem("API_KEY", token)
}

export function closeModal(modalName) {
   modalName.hide()
}

// Валідація та взаэмодія з токеном
function handleResponse(token) {
    if (isToken(token) && isValidEmail(emailInput.value)) {
        createVisitButton.style.display = "flex";
        logInButton.textContent = "Log Out";
        errorText.style.display = "none";
        title.textContent = "No items have been added";
        regForm.style.height = "120px";
        API_KEY = token;
        saveLoginState(true);
        saveToken(token)
        updateUI();
        closeModal(modal);
        loadAndDisplayCards()
    } else {
        errorText.style.display = "block";
        throw new Error("Wrong email or password");
    }
}

function saveLoginState(loggedIn) {
    localStorage.setItem("loggedIn", loggedIn);
}

export function getLoginState() {
    return JSON.parse(localStorage.getItem("loggedIn")) || false;
}

// Оновлення інтерфейсу
export function updateUI() {
    const loggedIn = getLoginState();
    title.textContent = "No items have been added"

    if (loggedIn) {
        createVisitButton.style.display = "flex";
        logInButton.textContent = "Log Out";
        regForm.style.height = "120px";
        cardForm.style.display = "block"
        cardsContainer.style.display = "grid"
        mainSection.style.minHeight = "470px"
        title.style.display = "block"
        filterContainer.style.display = "flex"
    } else {
        createVisitButton.style.display = "none";
        logInButton.textContent = "Log In";
        title.textContent = "Log In your account";
        title.style.display = "block"
        cardForm.style.display = "none"
        cardsContainer.style.display = "none"
        regForm.style.height = "80px";
        mainSection.style.minHeight = "120px"
        filterContainer.style.display = "none"
    }
}

// Та його перевірка
updateUI();

submitButton.addEventListener("click", () => {
    const formData = {
        email: emailInput.value,
        password: passwordInput.value,
    };

    fetch(API_URL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    })
        .then(response => response.text())
        .then(handleResponse)
        .catch(e => console.error(e));
});

logInButton.addEventListener("click", () => {
    const loggedIn = getLoginState();
    if (loggedIn) {
        saveLoginState(!loggedIn);
        updateUI();
    }
});