export const addCartButton = document.querySelector(".create-card");
export const createCardModal = new bootstrap.Modal(document.getElementById("createCardModal"));

document.addEventListener('DOMContentLoaded', () => {
    const dateInput = document.querySelector('input[type="date"]');
    const today = new Date().toISOString().split('T')[0];
        dateInput.setAttribute('min', today);
});