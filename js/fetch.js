import {emptyFiltersText} from "./filters.js";
import {getLoginState} from "./registration-form.js";
import {closeModal} from "./registration-form.js";
import {createCardModal} from "./modal.js";

export const CARDS_API_URL = "https://ajax.test-danit.com/api/v2/cards";
export const API_KEY = "API_KEY"

export function getToken(token) {
    return localStorage.getItem(token)
}

class Visit {
    constructor(data) {
        this.name = data.name;
        this.doctor = data.doctor;
        this.reason = data.reason;
        this.shortDescription = data.shortDescription;
        this.urgency = data.urgency;
        this.date = data.date
    }

    static fromForm() {
        let baseData = {
            name: document.getElementById('name-input').value,
            doctor: document.getElementById('doctor').value,
            reason: document.getElementById('reason').value,
            shortDescription: document.getElementById('short-description').value,
            urgency: document.getElementById('urgency').value,
            date: document.getElementById('date').value
        };

        switch (baseData.doctor) {
            case 'Cardiologist':
                baseData.pressure = document.getElementById('pressureField').value; // звичайний тиск
                baseData.bodyMassIndex = document.getElementById('bodyMassIndexField').value; // Індекс маси тіла
                baseData.pastDiseases = document.getElementById('pastDiseasesField').value; // перенесені захворювання серцево-судинної системи
                baseData.age = document.getElementById('ageField').value; // вік
                return new VisitCardiologist(baseData);

            case 'Dentist':
                baseData.lastVisitDate = document.getElementById('lastVisitDateField').value; // дата останнього відвідування
                return new VisitDentist(baseData);

            case 'Therapist':
                baseData.age = document.getElementById('ageField').value; // вік
                return new VisitTherapist(baseData);

            default:
                return new Visit(baseData);
        }
    }
}

class VisitDentist extends Visit {
    constructor(data) {
        super(data);
        this.lastVisitDate = data.lastVisitDate;
    }
}

class VisitCardiologist extends Visit {
    constructor(data) {
        super(data);
        this.pressure = data.pressure;
        this.bodyMassIndex = data.bodyMassIndex;
        this.pastDiseases = data.pastDiseases; // Исправленное название
        this.age = data.age;
    }
}

class VisitTherapist extends Visit {
    constructor(data) {
        super(data);
        this.age = data.age;
    }
}

function handleFormSubmit(event) {
    event.preventDefault();

    const visit = Visit.fromForm();

    createCard(visit)
        .then(newCard => {
            console.log('Карточка успешно создана:', newCard);
            loadAndDisplayCards();
        })
        .catch(error => {
            console.error('Ошибка при создании карточки:', error);
        });
}

document.addEventListener("DOMContentLoaded", function () {
    const doctorSelect = document.getElementById('doctor');

    if (!doctorSelect) return; // Если элемент select не найден, прекратите выполнение функции

    // Установите начальное состояние для дополнительных полей на основе текущего значения select
    updateAdditionalFieldsVisibility(doctorSelect.value);

    doctorSelect.addEventListener('change', function () {
        updateAdditionalFieldsVisibility(this.value);
    });
});

function updateAdditionalFieldsVisibility(selectedDoctor) {
    // Приховуємо всі додаткові поля
    const additionalFields = document.querySelectorAll('.additional-field');
    additionalFields.forEach(field => field.classList.add('hidden'));

    const lastVisitLabel = document.getElementById('label-last');
    if (lastVisitLabel) {
        if (selectedDoctor === 'Dentist') {
            lastVisitLabel.classList.remove('hidden');
        } else {
            lastVisitLabel.classList.add('hidden');
        }
    }

    switch (selectedDoctor) {
        case 'Cardiologist':
            document.getElementById('pressureField').classList.remove('hidden');
            document.getElementById('bodyMassIndexField').classList.remove('hidden');
            document.getElementById('pastDiseasesField').classList.remove('hidden');
            document.getElementById('ageField').classList.remove('hidden');
            break;
        case 'Dentist':
            document.querySelector('.last-visit-date-field').classList.remove('hidden');
            break;
        case 'Therapist':
            document.getElementById('ageField').classList.remove('hidden');
            break;
    }

    // Очищаем все выделенные ошибками поля
    clearAllErrorHighlights();
}

function clearAllErrorHighlights() {
    document.querySelectorAll('.input-group-text, .form-select').forEach(input => {
        input.style.borderColor = '';
        if (input.nextElementSibling && input.nextElementSibling.tagName.toLowerCase() === 'small') {
            input.nextElementSibling.remove();
        }
    });
}


const form = document.getElementById('card-form');
form.addEventListener('submit', handleFormSubmit);

export function createCard(data) {
    if (!areRequiredFieldsPresent(data)) {
        return Promise.reject(new Error('Не все обязательные поля заполнены'));
    }

    return fetch(CARDS_API_URL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken(API_KEY)}`
        },
        body: JSON.stringify(data)
    })
        .then(response => response.json())
        .then(card => {
            displayCard(card, document.getElementById('cards-container'));
            return card;
        });
}

export function getAllCards() {
    return fetch(CARDS_API_URL, {
        headers: {
            'Authorization': `Bearer ${getToken(API_KEY)}`
        }
    })
        .then(response => {
            if (!response.ok) {
                return response.text().then(text => {
                    throw new Error(text);
                });
            }
            return response.json();
        });
}

function deleteCardById(cardId) {
    return fetch(`${CARDS_API_URL}/${cardId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${getToken(API_KEY)}`
        }
    })
        .then(response => {
            if (!response.ok) {
                return response.text().then(text => {
                    throw new Error(text);
                });
            }
            setTimeout(loadAndDisplayCards, 600);  // Перезагрузить карточки после успешного удаления

            // проверяем, остались ли какие-либо карточки
            const container = document.getElementById('cards-container');
            const noItemsText = document.querySelector('.title');
            if (container.children.length === 0) {
                noItemsText.style.display = 'block';
            } else {
                noItemsText.style.display = 'none';
            }
        })
}

const toggleButton = document.createElement('button');

function displayCard(card, container) {
    const cardElement = document.createElement('div');
    cardElement.className = 'card';

    const buttonContainer = document.createElement("div");
    buttonContainer.className = "btn-group";
    buttonContainer.role = "group";
    buttonContainer.setAttribute("aria-label", "Basic example");

    let cardContent = `
        <h3 class="card-name">${card.name}</h3>
        <p class="card-doctor">Doctor: ${card.doctor}</p>
        <p class="card-reason">Purpose: ${card.reason}</p>
        <p class="card-date">Date: ${card.date}</p>
        <p class="card-shortDescription">Description: ${card.shortDescription || "none"}</p>
        <p class="urgency">Urgency: ${card.urgency}</p>
    `;

    if (card.doctor === 'Cardiologist') {
        cardContent += `
            <p class="card-pressure hidden-field">Pressure: ${card.pressure || 'N/A'}</p>
            <p class="card-bodyMassIndex hidden-field">Body mass index: ${card.bodyMassIndex || 'N/A'}</p>
            <p class="card-pastDiseases hidden-field">Recent illnesses: ${card.pastDiseases || 'N/A'}</p>
            <p class="card-age hidden-field">Age: ${card.age || 'N/A'}</p>
        `;
    } else if (card.doctor === 'Dentist') {
        cardContent += `
            <p class="card-lastVisitDate hidden-field">Last visit date: ${card.lastVisitDate || 'N/A'}</p>
        `;
    } else if (card.doctor === 'Therapist') {
        cardContent += `
            <p class="card-age hidden-field">Age: ${card.age || 'N/A'}</p>
        `;
    }

    cardElement.innerHTML = cardContent;
    cardElement.appendChild(buttonContainer);

    const localToggleButton = document.createElement('button');
    localToggleButton.type = "button";
    localToggleButton.textContent = 'Show More';
    localToggleButton.className = "btn btn-danger";
    localToggleButton.addEventListener('click', () => {
        const hiddenFields = cardElement.querySelectorAll('.hidden-field');
        cardElement.classList.toggle("expanded");
        if (hiddenFields.length) {
            hiddenFields.forEach(field => field.classList.remove('hidden-field'));
            localToggleButton.textContent = 'Hide';
        } else {
            const allFields = ['pressure', 'bodyMassIndex', 'pastDiseases', 'age', 'lastVisitDate'];
            allFields.forEach(field => {
                const el = cardElement.querySelector(`.card-${field}`);
                if (el) {
                    el.classList.add('hidden-field');
                }
            });
            localToggleButton.textContent = 'Show More';
        }
    });
    buttonContainer.appendChild(localToggleButton);

    const closeButton = document.createElement('button');
    const closeButtonIcon = document.createElement("img");
    closeButtonIcon.src = "img/delete.png";
    closeButtonIcon.className = "close-button-icon";
    closeButton.type = "button";
    closeButton.textContent = '';
    closeButton.className = 'close-button btn btn-danger';
    closeButton.addEventListener('click', () => {
        cardElement.style.animation = "fade-delete 1s ease";
        deleteCardById(card.id);
    });
    closeButton.appendChild(closeButtonIcon);
    buttonContainer.appendChild(closeButton);

    const editButton = document.createElement('button');
    const editButtonIcon = document.createElement("img");
    editButtonIcon.src = "img/pencil.png";
    editButtonIcon.className = "edit-button-icon";
    editButton.type = "button";
    editButton.textContent = '';
    editButton.className = 'edit-button btn btn-danger';
    editButton.addEventListener('click', () => {
        convertToEditable(cardElement, card);
    });
    editButton.appendChild(editButtonIcon);
    buttonContainer.appendChild(editButton);
    container.appendChild(cardElement);

    return cardElement;
}

function saveCardChanges(cardElement, cardId, cardData) {
    const updatedData = {
        id: cardId
    };

    const baseFields = ['name', 'doctor', 'reason', 'pressure', 'lastVisitDate', 'symptoms', 'shortDescription', 'urgency', 'age', 'date'];
    let specificFields = [];
    switch (cardData.doctor) {
        case 'Cardiologist':
            specificFields = ['pressure', 'bodyMassIndex', 'pastDiseases', 'age'];
            break;
        case 'Dentist':
            specificFields = ['lastVisitDate'];
            break;
        case 'Therapist':
            specificFields = ['age'];
            break;
        default:
            break;
    }
    const fields = [...baseFields, ...specificFields];

    fields.forEach(field => {
        const inputElement = cardElement.querySelector(`.editable-${field}`);
        if (inputElement && inputElement.value) {
            updatedData[field] = inputElement.value;
        } else {
            updatedData[field] = cardData[field] || null;
        }
    });

    fetch(`${CARDS_API_URL}/${cardId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken(API_KEY)}`
        },
        body: JSON.stringify(updatedData)
    })
        .then(response => response.json())
        .then(updatedCard => {
            const parentContainer = cardElement.parentElement;
            parentContainer.replaceChild(displayCard(updatedCard, parentContainer), cardElement);
        })
        .catch(error => {
            console.error('Ошибка при редактировании карточки:', error);
        });

    const localToggleButton = cardElement.querySelector('button[text="Show More"]') || cardElement.querySelector('button[text="Hide"]');
    if (localToggleButton) {
        localToggleButton.click();
    }
}

function convertToEditable(cardElement, cardData) {
    const localToggleButton = cardElement.querySelector('button[text="Show More"]') || cardElement.querySelector('button[text="Hide"]');
    if (localToggleButton) {
        localToggleButton.style.display = "none";
    }

    const existingSaveButton = cardElement.querySelector('.save-btn');
    if (existingSaveButton) {
        return;
    }

    const fields = ['name', 'doctor', 'reason', 'pressure', 'lastVisitDate', 'symptoms', 'shortDescription', 'urgency', 'age', 'bodyMassIndex', 'pastDiseases', 'date'];
    fields.forEach(field => {
        const currentContent = cardElement.querySelector(`.card-${field}`);

        if (field === 'doctor' || field === 'urgency') {
            return;
        }

        const inputElement = document.createElement('input');
        inputElement.className = `editable-${field} input-group-text`;

        // Устанавливаем тип и дополнительные свойства для инпутов, связанных с датами
        if (field === 'date') {
            inputElement.type = 'date';
            inputElement.min = new Date().toISOString().split('T')[0];  // Устанавливаем минимальное значение даты как сегодняшнее число
            inputElement.value = cardData[field] || new Date().toISOString().split('T')[0];
        } else if (field === 'lastVisitDate') {
            inputElement.type = 'date';
            inputElement.value = cardData[field] || '';
        } else {
            inputElement.type = 'text';
            inputElement.value = cardData[field] || '';
        }

        if (currentContent) {
            cardElement.replaceChild(inputElement, currentContent);
        } else {
            console.warn(`Не удалось найти элемент для поля ${field}`);
        }
    });

    const saveButton = document.createElement('button');
    saveButton.className = "btn btn-secondary save-btn";
    saveButton.textContent = 'Save changes';
    saveButton.classList.add('btn-success');
    saveButton.addEventListener('click', () => {
        saveCardChanges(cardElement, cardData.id, cardData);
        toggleButton.style.display = "block";
    });

    cardElement.appendChild(saveButton);

    if (convertToEditable) {
        toggleButton.style.display = "none";
    }
}

function displayAllCards(cards) {
    const container = document.getElementById('cards-container');
    container.innerHTML = '';  // очищаем контейнер

    const noItemsText = document.querySelector('.title');
    if (cards.length === 0) {
        noItemsText.style.display = 'block';
        emptyFiltersText.textContent = ""
    } else {
        emptyFiltersText.classList.remove("show")
        emptyFiltersText.classList.add("hide")
        emptyFiltersText.textContent = "Cards not Found"
        if (getLoginState()) {
            noItemsText.style.display = 'none';
        } else {
            noItemsText.style.display = 'block';
        }
    }

    cards.forEach(card => {
        displayCard(card, container);
    });
}

export function loadAndDisplayCards() {
    getAllCards()
        .then(cards => {
            displayAllCards(cards);
        })
        .catch(error => {
            console.error('Ошибка при загрузке карточек:', error);
        });
}

window.addEventListener("DOMContentLoaded", loadAndDisplayCards)

function areRequiredFieldsPresent(data) {
    let missingFields = [];

    const requiredFields = {
        name: 'name',
        doctor: 'doctor',
        reason: 'reason',
        date: 'date',
        urgency: 'urgency'
    };

    for (let key in requiredFields) {
        if (!data[key]) {
            missingFields.push(key);
        }
    }

    switch (data.doctor) {
        case 'Cardiologist':
            ['pressure', 'bodyMassIndex', 'pastDiseases', 'age'].forEach(field => {
                if (!data[field]) {
                    missingFields.push(field);
                }
            });
            break;
        case 'Dentist':
            if (!data.lastVisitDate) {
                missingFields.push('lastVisitDate');
            }
            break;
        case 'Therapist':
            if (!data.age) {
                missingFields.push('age');
            }
            break;
        default:
            console.warn(`Неизвестный тип врача: ${data.doctor}`);
    }

    if (missingFields.length > 0) {
        highlightMissingFields(missingFields);
        return false;
    }

    closeModal(createCardModal)

    return true;
}

function highlightMissingFields(fields) {
    fields.forEach(field => {
        const inputElement = document.querySelector(`.editable-${field}`);
        if (inputElement) {
            inputElement.style.borderColor = 'red';

            // Проверяем, существует ли уже элемент <small> рядом с полем ввода
            if (!inputElement.nextElementSibling || inputElement.nextElementSibling.tagName.toLowerCase() !== 'small') {
                const errorMessage = document.createElement('small');
                errorMessage.style.color = 'red';
                errorMessage.textContent = 'This field is required';
                inputElement.insertAdjacentElement('afterend', errorMessage);
            }
        }
    });
}

function removeHighlightingAndError() {
    document.querySelectorAll('.input-group-text, .form-select').forEach(input => {
        input.addEventListener('input', function () {
            if (this.value) {
                this.style.borderColor = '';
                if (this.nextElementSibling && this.nextElementSibling.tagName.toLowerCase() === 'small') {
                    this.nextElementSibling.remove();
                }
            }
        });
    });
}

removeHighlightingAndError();







